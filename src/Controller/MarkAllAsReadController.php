<?php

namespace Drupal\mark_all_as_read\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\flag\FlagServiceInterface;
use Drupal\message\Entity\Message;

/**
 * Controller for the "Makr all as read" link.
 */
class MarkAllAsReadController extends ControllerBase {

  /**
   * The flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  private $flagService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $flagService = $container->get('flag');
    return new static($flagService);
  }

  /**
   * Creates an MarkAllAsReadController object.
   *
   * @param \Drupal\flag\FlagServiceInterface $flagService
   *   The flag service.
   */
  public function __construct(FlagServiceInterface $flagService) {
    $this->flagService = $flagService;
  }

  /**
   * Mark all messages as readed (flag all messages).
   *
   * @return Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the front page.
   */
  public function markAllAsRead() {
    $flag = $this->flagService->getFlagById('mark_message');
    $flag_bundles = $flag->getBundles();

    $mids = $this->entityTypeManager()->getStorage('message')->getQuery()->execute();
    $messages = Message::loadMultiple($mids);

    foreach ($messages as $message) {
      // Check that a message bundle is allowed by the flag.
      if (!empty($flag_bundles) && !in_array($message->bundle(), $flag_bundles)) {
        continue;
      }
      // Check that a message isn't flagged.
      if ($this->flagService->getFlagging($flag, $message) == NULL) {
        $this->flagService->flag($flag, $message);
      }
    }

    return $this->redirect('<front>');
  }

}
