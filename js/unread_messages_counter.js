/**
 * @file
 * JavaScript behaviors for refresh anread message counter.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Refresh anread message counter.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.UnreadMessageCounter = {
    attach: function (context) {
      $('.flag-mark-message a').once('unread-message-counter').click(function (event) {
        var counter = $(".view-unread-messages-count .view-header");
        if ($(this).parent().hasClass("action-flag")) {
          var new_value = parseInt(counter.html()) - 1;
          counter.html(new_value);
        }
        else if ($(this).parent().hasClass("action-unflag")) {
          var new_value = parseInt(counter.html()) + 1;
          counter.html(new_value);
        }
      });
    }
  };

})(jQuery, Drupal);
